import { Component } from '@angular/core';

@Component({
  selector: 'bank-mfe-authentication-root',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent {}
